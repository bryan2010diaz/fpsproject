﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {
	
	private Animator anim;
	private CharacterController controller;
	
	// Use this for initialization
	void Start () {
		anim = gameObject.GetComponentInChildren<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
		if (Input.GetKey ("w")) {
			anim.SetInteger ("AnimPar", 1);
		} else {
			anim.SetInteger ("AnimPar", 0);
		}
	}
}
