﻿public var MoveAmount : float = 1;

public var MoveSpeed : float = 2;

public var Gun : GameObject;

public var MoveOnX : float;

public var MoveOnY : float;

public var DefaultPos : Vector3;

public var NewGunPos : Vector3;

function Start(){

DefaultPos = transform.localPosition;

}


function Update(){

MoveOnX= Input.GetAxis("Move X") *Time.deltaTime * MoveAmount;

MoveOnY= Input.GetAxis("Move Y") *Time.deltaTime * MoveAmount;

NewGunPos= new Vector3 (DefaultPos.x + MoveOnX, DefaultPos.y + MoveOnY, DefaultPos.z);

Gun.transform.localPosition = Vector3.Lerp(Gun.transform.localPosition, NewGunPos, MoveSpeed* Time.deltaTime);

}